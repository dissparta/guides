![banner](https://1.bp.blogspot.com/-ELq4Mda-0b0/VYR7h3p3VhI/AAAAAAAADHY/An9eps_M7Kg/s1600/logo%2Bjdownloader%2B2.jpg)

------------------------**Download JDownloader2**-----------------------------------

Windows, Mac: <https://jdownloader.org/jdownloader2>

Linux: <https://flathub.org/apps/details/org.jdownloader.JDownloader> (make sure Flatpak is installed)

------------------------**Configuration**-----------------------------------

- Go to settings
- General tab
- Change "Max. Chunks per Download" from **1** to **20**

This will make your download faster!

------------------------**How to use??**-----------------------------------

1. Download the file as you normally do with a browser.
2. Cancel the download, right click on it and copy the link address.
3. Right click somewhere in JD2, click on "Add links" and paste the link address in the box that pops up.
4. Go to "Link Grabber" tab and right click the file if JD2 is able to find it.
5. Click on "Start downloads"

------------------------**Why is this important? When should I do this?**-----------------------------------

- Your browser isn't very good at downloading a large file or multiple files at the same time.
- If you get a powercut while downloading something from the browser, it won't be saved and you'll lose all progress you made. This is not true for JDownloader2 as it saves the progess, you'll be able to resume a download reliably whenever you turn on the computer afterwards.
- Usually, you should get better speeds while downloading using JDownloader2 than you'll get by using Firefox/Chrome.
- It is much easier to have multiple downloads running at the same time in JDownloader2 compared to Firefox/Chrome because JD2 uses less resources.

------------------------**BONUS**-----------------------------------

JDownloader2 has a feature where it can start grabbing links that you copy on your clipboard, you can use it when there are multiple files that you wanna download, just keep copying the link addresses of the files and it will automatically be added to the LinkGrabber

This is the clipboard grabber, you can use it to toggle this behavior on and off.

![image](/uploads/23655d8af6d162b380973c88281d3de1/image.png)
